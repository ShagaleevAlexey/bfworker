import os
import argparse
from multiprocessing import Process, Pool
import subprocess
from time import sleep
import math

from brainflayerWorker import BFConfig, brainflayer_processing

parser = argparse.ArgumentParser(description='BTC worker')
parser.add_argument('bffile', metavar='bffile', type=str, nargs='+', help='path of bloom filter file need for work')
parser.add_argument('pdir', metavar='pdir', type=str, nargs='+', help='path of passwords files need for work')
parser.add_argument('-brainflayer', metavar='brainflayer', type=str, nargs='+', default=['../brainflayer'], help='directory with braiflayer software')
parser.add_argument('-threads', metavar='threads', type=int, nargs='+', default=[2], help='count thread for work')

args = parser.parse_args()

bffile_path = args.bffile[0]
passwords_dir = args.pdir[0]
brainflayer_path = args.brainflayer[0]
threads_count = args.threads[0]

assert os.path.exists(bffile_path), f'Bloom filter file \'{bffile_path}\' not found'
assert os.path.exists(passwords_dir), f'Passwords dir \'{passwords_path}\' not found'
from os import walk

passwords_files = []

for (dirpath, dirnames, filenames) in walk(passwords_dir):
    passwords_files.extend([os.path.join(passwords_dir, name) for name in filenames])
    break

assert len(passwords_files) > 0

print(f'[0] Find {len(passwords_files)} passwords files')

assert len(passwords_files) >= threads_count, f'Passwords file is much more'

def generate_configs():
    configs = []

    for passfile in passwords_files:
        config = BFConfig(brainflayer_path, bffile_path, passfile)

        configs.append(config)

    return configs

pool = Pool(threads_count)
workers = pool.map(brainflayer_processing, generate_configs())

pool.close()
pool.join()

print(f'[5]Finished')
