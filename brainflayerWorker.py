from subprocess import Popen, PIPE
from time import sleep
import os

class BFConfig:
    brainflayer_path = ''
    bffile_path = ''
    pfile_path = ''

    def __init__(self, brainflayer_path : str, bffile_path : str, pfile_path : str):
        self.brainflayer_path = brainflayer_path
        self.bffile_path      = bffile_path
        self.pfile_path       = pfile_path

start_process_format = '{0}/brainflayer -v -b {1} -i {2} -o results/r_{3}'

def brainflayer_processing(config : BFConfig):
    cmd = start_process_format.format(config.brainflayer_path, config.bffile_path, config.pfile_path, os.path.basename(config.pfile_path))
    print(f'Processing {config.pfile_path}')

    process = Popen(cmd, shell=True)
    # (output, err) = process.communicate()
    exit_code = process.wait()
